document.addEventListener('DOMContentLoaded', (event) => {

    let main = document.querySelector('main');
    let background = document.querySelector('.background');
    let themeBtn = document.querySelector('.theme-button');
    let todoInputContainer = document.querySelector('.input-todo');
    let submitDataBtn = document.querySelector('#submit-data');
    let listContainer = document.querySelector('.list-holder');
    let taskTodo = document.querySelector('#task-todo');
    let inputData = document.querySelector('#todo-data');
    let links = document.querySelector('.links');
    let counterContainer = document.querySelector('.counter-and-clear-operation');
    let clear = document.querySelector('#clear-completed');

    let counter = document.createElement('span');
    counterContainer.insertBefore(counter, counterContainer.querySelector('.counter'));
    counter.id = 'count';
    counter.innerHTML = 0;

    let flag = true;

    themeBtn.addEventListener('click', () => {
        if (flag) {
            flag = false;
            main.style.backgroundColor = "var(--dark-Very-Dark-Blue)";
            background.style.backgroundImage = "url('images/bg-mobile-dark.jpg')";
            themeBtn.style.backgroundImage = "url('images/icon-sun.svg')";
            todoInputContainer.style.backgroundColor = "var(--dark-Very-Dark-Desaturated-Blue)";
            listContainer.style.backgroundColor = "var(--dark-Very-Dark-Desaturated-Blue)";
            taskTodo.style.color = "var(--dark-Light-Grayish-Blue)";
            links.style.backgroundColor = "var(--dark-Very-Dark-Desaturated-Blue)";
            inputData.style.color = "var(--light-Dark-Grayish-Blue)";

        } else {
            flag = true;
            main.style.backgroundColor = "var(--light-Very-Light-Grayish-Blue)";
            background.style.backgroundImage = "url('images/bg-mobile-light.jpg')";
            themeBtn.style.backgroundImage = "url('images/icon-moon.svg')";
            todoInputContainer.style.backgroundColor = "var(--light-Very-Light-Gray)";
            todoInputContainer.style.color = "var(--dark-Very-Dark-Desaturated-Blue)";
            listContainer.style.backgroundColor = "var(--light-Very-Light-Gray)";
            taskTodo.style.color = "var(--light-Very-Dark-Grayish-Blue)";
            links.style.backgroundColor = "var(--light-Very-Light-Gray)";
            // inputData.style.color = "var(--dark-Light-Grayish-Blue)";
        }
    });

    todoInputContainer.addEventListener('keypress', (e) => {
        if (e.keyCode === 13) {

            let data = inputData.value;
            inputData.value = "";

            if (data != "") {
                let newList = document.createElement('div');
                let checkbox = createCheckbox();
                let todoText = document.createElement('span');

                let deleteBtn = createDelete();

                newList.className = 'task';
                deleteBtn.className = 'deleteTask';
                checkbox.className = 'completeTask';
                todoText.className = 'taskData';
                todoText.style.color = 'var(--light-Very-Dark-Grayish-Blue)';
                todoText.style.fontWeight = 'bold';

                newList.style.display = 'flex';
                newList.style.justifyContent = 'space-between';
                newList.style.alignItems = 'center';
                newList.style.height = '50px';
                newList.style.marginLeft = '10px';
                newList.style.marginRight = '10px';

                newList.appendChild(checkbox);
                todoText.appendChild(document.createTextNode(data));
                newList.appendChild(todoText);
                newList.appendChild(deleteBtn);

                taskTodo.appendChild(newList);
                taskTodo.style.borderBottom = "1px solid var(--light-Light-Grayish-Blue)";

                counter.innerHTML = countActiveTasks();


            }
        }

    });

    taskTodo.addEventListener('click', (e) => {
        if (e.target.classList.contains('deleteTask')) {
            if (confirm('Are you sure ?')) {
                var div = e.target.parentElement;
                taskTodo.removeChild(div);
                counter.innerHTML = countActiveTasks();
            }
        }
    });

    taskTodo.addEventListener('change', (e) => {
        let div = e.target.parentElement;
        let taskData = div.querySelector('.taskData');
        let checkbox = div.querySelector('.completeTask');

        if (checkbox.checked) {
            taskData.style.textDecoration = 'line-through';
            checkbox.style.backgroundImage = 'url("images//icon-check.svg"), var(--background)';
            checkbox.style.backgroundSize = "100% 100%";
            checkbox.backgroundPosition = 'center';
            counter.innerHTML = countActiveTasks();

        } else {
            taskData.style.textDecoration = 'none';
            checkbox.style.backgroundImage = 'none';
            counter.innerHTML = countActiveTasks();
        }
    })

    links.querySelector('#all').addEventListener('click', (e) => {
        let tasks = taskTodo.children;
        for (let i = 0; i < tasks.length; i++) {
            tasks[i].style.display = 'flex';
        }
    });

    links.querySelector('#active').addEventListener('click', (e) => {
        let tasks = taskTodo.children;
        for (let i = 0; i < tasks.length; i++) {
            if (tasks[i].style.display == 'none') {
                tasks[i].style.display = 'flex';
            }
            if ((tasks[i].querySelector('.completeTask').checked)) {
                tasks[i].style.display = 'none';
            }
        }
    });

    links.querySelector('#completed').addEventListener('click', (e) => {
        let tasks = taskTodo.children;
        for (let i = 0; i < tasks.length; i++) {
            if (tasks[i].style.display == 'none') {
                tasks[i].style.display = 'flex';
            }
            if (!(tasks[i].querySelector('.completeTask').checked)) {
                tasks[i].style.display = 'none';
            }
        }
    });

    clear.addEventListener('click', () => {
        let tasks = taskTodo.children;
        for (let i = 0; i < tasks.length; i++) {
            if ((tasks[i].querySelector('.completeTask').checked)) {
                tasks[i].remove();
            }
        }
    })

    function countActiveTasks() {
        let tasks = taskTodo.children;
        let count = 0;
        for (let i = 0; i < tasks.length; i++) {
            if (!(tasks[i].querySelector('.completeTask').checked)) {
                count++;
            }
        }
        return count;
    }

});


function createCheckbox() {
    let checkbox = document.createElement('input');

    checkbox.setAttribute('type', 'checkbox');
    checkbox.style.width = '25px';
    checkbox.style.height = '25px';
    checkbox.style.backgroundColor = 'inherit';
    checkbox.style.borderRadius = '50%';
    checkbox.style.vericalAlign = 'middle';
    checkbox.style.border = 'solid 2px var(--light-Light-Grayish-Blue)';
    checkbox.style.appearance = 'none';
    checkbox.style.outline = 'none';
    checkbox.style.cursor = 'pointer';
    return checkbox;
}

function createDelete() {
    let deleteBtn = document.createElement('buton');
    deleteBtn.style.background = "url('images/icon-cross.svg') no-repeat center ";
    deleteBtn.style.height = '20px';
    deleteBtn.style.width = '20px';
    deleteBtn.style.backgroundSize = '100% 100%';
    deleteBtn.style.backgroundColor = 'inherit';
    deleteBtn.style.border = 'none';
    return deleteBtn;
}

